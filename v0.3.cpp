#include<iostream>
#include<cstring>
using namespace std;

void CharCount(FILE *file)//统计字符数量
{
    int charcount=0;
    char ch;
    ch=fgetc(file);
    while(!feof(file))
    {
        charcount++;
        ch=fgetc(file);
    }
    cout<<"字符数:"<<charcount<<endl;
    rewind(file);
}

void WordCount(FILE *file)//统计单词数量
{
    int wordcount=0;
    char ch;
    ch=fgetc(file);
    while(!feof(file))
    {
        if(isalnum(ch))
        {
            while(isalnum(ch)||ch=='-')
            {
                ch=fgetc(file);
            }
            wordcount++;
        }

        ch=fgetc(file);
    }
    cout<<"单词数:"<<wordcount<<endl;
    rewind(file);
}

void SentenceCount(FILE *file)//统计句子数
{
    int sentenceCount=0;
    char ch;
    ch=fgetc(file);
    while(!feof(file))
    {
        if(ch=='.'||ch=='!'||ch=='?')
        {
            sentenceCount++;
        }
        ch=fgetc(file);
    }
    cout<<"句子数:"<<sentenceCount<<endl;
    rewind(file);
}

int main(int argc, char* argv[])
{
    FILE *file;
    file = fopen(argv[argc-1],"r");
    if(file == NULL){
    	cout<<"不能打开此文件！"<<endl;
		exit(0); 
	}
	for(int i = 1; i < argc-1; i++){
		if(!strcmp(argv[i],"-c")){
			CharCount(file);
		}else if(!strcmp(argv[i],"-w")){
			WordCount(file);
		}else if(!strcmp(argv[i],"-s")){
			SentenceCount(file);
		}else{
			cout<<argv[i]<<" 不是合法命令"<<endl; 
		}
	}
    return 0;
}

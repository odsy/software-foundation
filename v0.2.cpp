#include<iostream>
#include<cstring>
using namespace std;

void CharCount(FILE *file)//统计字符数量
{
    int charcount=0;
    char ch;
    ch=fgetc(file);
    while(!feof(file))
    {
        charcount++;
        ch=fgetc(file);
    }
    cout<<"字符数:"<<charcount<<endl;
    rewind(file);
}

int main(int argc, char* argv[])
{
    FILE *file;
    file = fopen(argv[argc-1],"r");
    if(file == NULL){
    	cout<<"不能打开此文件！"<<endl;
		exit(0); 
	}
	for(int i = 1; i < argc-1; i++){
		if(!strcmp(argv[i],"-c")){
			CharCount(file);
		}else{
			cout<<argv[i]<<" 不是合法命令"<<endl; 
		}
	}
    return 0;
}

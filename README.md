# 软件基础

#### 介绍
软件基础第3次作业——命令行文本计数统计程序

#### 软件架构
使用C++编写


#### 项目的简介及其相关的用法

1.        
2.  v0.3.exe -c 纯英文文本.txt    统计字符数
3.  v0.3.exe -s 纯英文文本.txt    统计句子数
4.  v0.3.exe -w 纯英文文本.txt    统计单词数
5.  也可任意2者、3者组合，分别得出该文本的字符、句子、单词数

#### 文件列表及其相关说明

1.  v0.1.cpp  空项目
2.  v0.2.cpp  项目完成基础功能
3.  v0.3.cpp  项目完成扩展功能（加分项）
4.  纯英文文本.txt  测试文本

#### 例程运行及其相关结果

```
C:AWINDOWS\system32\cmd.exe

Microsoft Windows [版本10. 0. 19043. 1288 ]

(c) Microsoft Corporation. 保留所有权利。

C: \Users \Lenovo>cd C: \Users\Lenovo\Desktop\新建文件夹

C: \Users \Lenovo\Desktop\新建文件夹>v0.2.exe -c纯英文文本. txt字符数:413

C: \Users \Lenovo\Desktop\新建文件夹>v0.2. exe -s纯英文文本. txt-s不是合法命令

C: \Users \Lenovo\Desktop\新建文件夹>v0.3. exe -c纯英文文本. txt字符数:413

C: \Users \Lenovo\Desktop\新建文件夹>v0.3.exe -s纯英文文本. txt句子数:4

C: \Users \Lenovo\Desktop\新建文件夹>v0.3. exe -w纯英文文本. txt单词数:77

```



